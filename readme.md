# icms-orm

This project groups ORM class definitions for database tables used by the new iCMS webapps. 

# Using within client code

It's bad but certain elements need to be initialized before eg. model classes are imported. 
There are quite some methods in the main package that are meant to return the names of DB binds or schemas. 
These methods are invoked when defining the model classes. Schema / bind names can be overridden by replacing these
methods prior to importing any model classes. 

# Using Alembic

## Importing from icms-orm within migration scripts
Python path might not be set up to include the icms-orm itself, particularly when running something like `alembic history`.
Just paste that at the top of a problematic revision: 

```python
import sys
import os
if '%s/../..' % os.path.dirname(os.path.realpath(__file__)) not in sys.path:
    sys.path.insert('%s/../..' % os.path.dirname(os.path.realpath(__file__)))
```
    
## Database URL
It is stored in `alembic.ini`. Would be nice to put it in some deployment-specific file if possible to do so cleanly.

  
   