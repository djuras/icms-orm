from sqlalchemy.types import String, Integer, Date, Enum, DateTime
from datetime import date
from sqlalchemy import func, Column, and_, or_
from datetime import timedelta

from .common import PeopleBaseMixin
from icms_orm import DeclarativeBase, PseudoEnum
from .people import MoData


class InstStatusValues(PseudoEnum):
    YES = 'Yes'
    NO = 'No'
    FOR_REFERENCE = 'ForReference'
    ASSOCIATED = 'Associated'
    LEAVING = 'Leaving'
    COOPERATING = 'Cooperating'

__inst_status_options = (INST_STATUS_YES, INST_STATUS_NO, INST_STATYS_FOR_REFERENCE, INST_STATUS_ASSOCIATED,
                         INST_STATUS_LEAVING, INST_STATUS_COOPERATING) = \
    [InstStatusValues.YES, InstStatusValues.NO, InstStatusValues.FOR_REFERENCE, InstStatusValues.ASSOCIATED, InstStatusValues.LEAVING, InstStatusValues.COOPERATING]
inst_status_enum = Enum(*__inst_status_options, name='inst_status')


class Institute(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'InstData'

    code = Column('InstCode', String(40), primary_key=True, autoincrement=False)
    address = Column('AddrFull', String(255))
    name = Column('NameFull', String(255))
    shortName = Column('NameShort', String(127))
    nameSign = Column('NameSign', String(255), nullable=True)
    country = Column('Country', String(255))
    town = Column('Town', String(40))
    cernInstId = Column('InstId', Integer)
    instNb = Column('InstNb', Integer)
    gbInstId = Column('InstGBid', String(40))
    secId = Column('SECid', Integer)

    dateCmsIn = Column('DateCMSIn', Date, nullable=True, default=None)
    dateCmsOut = Column('DateCMSOut', Date, nullable=True, default=None)
    cmsStatus = Column('CMSStatus', inst_status_enum, default='Yes')
    dateCreate = Column('DateCreate', DateTime)

    # cbiCmsId = None
    # cbiPerson = None
    # cbdCmsId = None
    # cbdPerson = None
    # cbd2CmsId = None
    # cbd2Person = None

    delegateCmsId = None
    delegatePerson = None
    delegateDate = Column('DateDelegate', Date)

    wwwInst = Column('wwwInst', String(255))
    wwwCms = Column('wwwCMS', String(255))

    funding = Column('Funding', String(16), nullable=True, default=None)
    fundingAgency = Column('FA', String(40), nullable=True, default=None)
    fundingAgency2 = Column('FA2', String(40), nullable=True, default=None)

    usaOrder = Column('USAOrder', String(40), nullable=True)
    usState = Column('State', String(40), nullable=True)
    domain = Column('Domain', String(40), nullable=True)
    domainA = Column('Domaina', String(40), nullable=True)
    domainB = Column('Domainb', String(40), nullable=True)
    domainC = Column('Domainc', String(40), nullable=True)
    domainD = Column('Domaind', String(40), nullable=True)
    spiresIcn = Column('SpiresICN', String(255), nullable=True)
    spiresIcnA = Column('SpiresICNa', String(255), nullable=True)
    spiresIcnB = Column('SpiresICNb', String(255), nullable=True)
    spiresIcnC = Column('SpiresICNc', String(255), nullable=True)
    spiresIcnD = Column('SpiresICNd', String(255), nullable=True)

    #     UNUSED AS OF YET:
    #
    #     CBIshared = Column( Enum( '0', '1' ) )
    #     CBITeamAcc = Column( String )
    #     isGroup = Column( Enum( 'Yes', 'No' ) )
    #     DateModif = Column( DateTime )
    #     Email = Column( String )
    #     Fax1 = Column( String )
    #     Fax2 = Column( String )
    #     FTE = Column( Float )
    #     FTEremark = Column( String )
    #     MOdone = Column( Enum( '0', '1' ) )
    #     MOyear = Column( String )
    #     MsNmsObs = Column( Enum( 'MemberState', 'NonMemberState', 'Observer' ) )
    #     Phone1 = Column( String )
    #     Phone2 = Column( String )
    #     Remarks = Column( String )
    #     RemarkSecr = Column( String )
    #     Telex = Column( String )
    #     ZIPcode = Column( String )
    #     Statecode = Column( String )
    #     IRCgroup = Column( String )
    #     CBITeamAcc2 = Column( String )
    #     lat = Column( String )
    #     lng = Column( String )
    #     reallat = Column( String )
    #     reallng = Column( String ))
    #     IRCid = Column( Integer )

    def __unicode__(self):
        return '%s (%s)' % (self.code, self.country)
