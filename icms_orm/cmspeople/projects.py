from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
import sqlalchemy
from sqlalchemy.types import Integer, SmallInteger, String, Enum, Text
from type_decorators import StringInt, StringFloat, StringBool
from datetime import date

from .common import PeopleBaseMixin
from icms_orm import DeclarativeBase
from .institutes import Institute
from .people import Person


class Project(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'ServiceProjectsName'
    # todo: incomplete though remaining fields might be redundant already

    id = Column('PrKeyPF', Integer, primary_key=True, autoincrement=True)
    code = Column('ServicePrjId', String(30), nullable=False)
    name = Column('ServicePrjName', String(255), nullable=False)
    year = Column('Year', StringInt(length=4), default=2099, nullable=False)

    def __str__(self):
        return 'Project {0} [code {2}] from {1}'.format(self.name, self.year, self.code)


class ProjectOld(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'ServiceProjectsName_OLD'
    #todo: incomplete though remaining fields might be redundant already

    code = Column('ServicePrjId', String(30), nullable=False, primary_key=True)
    name = Column('ServicePrjName', String(255), nullable=False)
    year = Column('Year', StringInt(length=4), default=2099, nullable=False)

    def __str__(self):
        return 'Project_old {0} [code {2}] from {1}'.format(self.name, self.year, self.code)


class ProjectManagers(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'MngPrjsRel'

    id = Column('PrKeyPF', Integer, primary_key=True, autoincrement=True)
    cmsId = Column('CMSid', SmallInteger, ForeignKey(Person.cmsId), nullable=False, default=0)
    manager1 = relationship(Person, foreign_keys=[cmsId])
    cmsId2 = Column('CMSid2', SmallInteger, ForeignKey(Person.cmsId), nullable=False, default=0)
    manager2 = relationship(Person, foreign_keys=[cmsId2])
    cmsId3 = Column('CMSid3', SmallInteger, ForeignKey(Person.cmsId), nullable=False, default=0)
    manager3 = relationship(Person, foreign_keys=[cmsId3])
    cmsId4 = Column('CMSid4', SmallInteger, ForeignKey(Person.cmsId), nullable=False, default=0)
    manager4 = relationship(Person, foreign_keys=[cmsId4])
    cmsIdRoot = Column('CMSidRoot', SmallInteger, ForeignKey(Person.cmsId), nullable=False, default=0)
    manager = relationship(Person, foreign_keys=[cmsIdRoot])
    year = Column('Year', StringInt(length=4), nullable=False, default=2009)
    project_code = Column('ServicePrjId', String(30), nullable=False, default='')
    # The DB actually only has a FK on code that references code from ServiceProjectsName_OLD
    project = relationship(Project, backref='managers', foreign_keys=[project_code, year],
                           primaryjoin=sqlalchemy.and_(year == Project.year, project_code == Project.code))
    url = Column('Url', String(255), nullable=False, default='')

    def __str__(self):
        return 'Project managers for {0}: {1}, {2}. {3}, {4}, {6} [ROOT] in {5}'.format(self.project_code, self.cmsId,
            self.cmsId2, self.cmsId3, self.cmsId4, self.year, self.cmsIdRoot)


class Pledge(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'PeopleTasksRel'

    id = Column('PrKeyPF', Integer, primary_key=True)
    cmsId = Column('CMSid', SmallInteger, ForeignKey(Person.cmsId))
    # person = relationship(Person, backref='pledges')
    instCode = Column('InstCode', String(40), ForeignKey(Institute.code))
    # institute = relationship(Institute, backref='pledges')

    projectId = Column('ServicePrjId', String(30))  #, ForeignKey(Project.projectId))
    activityId = Column('ServiceActId', String(30)) #, ForeignKey(Activity.activityId))
    taskId = Column('ServiceTskId', String(30))     #, ForeignKey(Task.taskId))

    year = Column('Year', StringInt(length=4), default=date.today().year)
    amount = Column('Amount', StringFloat(length=6), default=0.0)
    total = Column('Total', StringFloat(length=6), default=0.0)
    totalAccepted = Column('TotalAccepted', StringFloat(length=6), default=0.0)
    totalShift = Column('TotalShift', StringFloat(length=6), default=0.0)
    percentage = Column('Percentage', SmallInteger, default=100)

    accepted = Column('Accepted', Enum('NO', 'PENDING', 'YES', 'REJECTED', 'PARTIAL', name='accepted_enum'))

    isGlobal = Column('Global', StringBool, default=False)
    isLocked = Column('Locked', StringBool, default=False)

    autoPledged = Column('AutoPledged', String(10))
    subsystem = Column('SubSystem', String(20))
    shiftIds = Column('ShiftIds', Text, default='')
