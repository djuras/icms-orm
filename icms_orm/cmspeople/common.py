from icms_orm import cms_people_bind_key
from icms_orm import cms_people_schema_name
from datetime import date, datetime
import re


class PeopleBaseMixin(object):
    __bind_key__ = cms_people_bind_key()
    __table_args__ = {'schema': cms_people_schema_name()}


class Coeffs(object):
    @staticmethod
    def get_author_due(year=date.today().year):
        if year <= 2015:
            return 4.5
        else:
            return 3

    @staticmethod
    def get_applicant_due(year=date.today().year):
        return 6


def date_from_string(date_string):
    prog1 = re.compile(r'\d{2}/\d{2}/\d{4}')
    prog2 = re.compile(r'\d{2}/\d{2}/\d{2}')

    if prog1.match(date_string):
        return datetime.strptime(date_string, '%d/%m/%Y').date()
    elif prog2.match(date_string):
        return datetime.strptime(date_string, '%d/%m/%y').date()
    return None