#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy import Column, String, Integer, DateTime, SmallInteger
from icms_orm import news_bind_key, news_schema_name, DeclarativeBase, PseudoEnum
import re

YES = True
NO = False


class NewsBaseMixin(object):
    __bind_key__ = news_bind_key()
    __table_args__ = {'schema': news_schema_name()}


class ChannelIdValues(PseudoEnum):
    GENERAL = 18
    JOBS = 27
    CONFERENCES = 31


class Channel(NewsBaseMixin, DeclarativeBase):
    __tablename__ = 'CHANNELS'

    id = Column('CHANNEL_ID', Integer(), primary_key=True, nullable=NO, default=None)
    title = Column('TITLE', String(255), nullable=NO, default=None)
    description = Column('DESCRIPTION', String(255), nullable=YES, default=None)
    loc_string = Column('LOCSTRING', String(255), nullable=YES, default=None)
    site = Column('SITE', String(255), nullable=YES, default=None)
    creator = Column('CREATOR', String(255), nullable=YES, default=None)
    publisher = Column('PUBLISHER', String(255), nullable=YES, default=None)
    language = Column('LANGUAGE', String(255), nullable=YES, default=None)
    format = Column('FORMAT', String(255), nullable=YES, default=None)
    image_id = Column('IMAGE_ID', Integer(), nullable=YES, default=None)
    textinput_id = Column('TEXTINPUT_ID', Integer(), nullable=YES, default=None)
    copyright = Column('COPYRIGHT', String(255), nullable=YES, default=None)
    rating = Column('RATING', String(255), nullable=YES, default=None)
    cloud_id = Column('CLOUD_ID', Integer(), nullable=YES, default=None)
    generator = Column('GENERATOR', String(255), nullable=YES, default=None)
    docs = Column('DOCS', String(255), nullable=YES, default=None)
    ttl = Column('TTL', Integer(), nullable=YES, default=None)
    last_updated = Column('LAST_UPDATED', DateTime(), nullable=YES, default=None)
    last_build_date = Column('LAST_BUILD_DATE', DateTime(), nullable=YES, default=None)
    pub_date = Column('PUB_DATE', DateTime(), nullable=YES, default=None)
    update_persion = Column('UPDATE_PERIOD', String(255), nullable=YES, default=None)
    update_frequency = Column('UPDATE_FREQUENCY', Integer(), nullable=YES, default=None)
    update_base = Column('UPDATE_BASE', DateTime(), nullable=YES, default=None)


class NewsItem(NewsBaseMixin, DeclarativeBase):
    __tablename__ = 'ITEMS'

    id = Column('ITEM_ID', Integer(), primary_key=True, nullable=False, default=None)
    channel_id = Column('CHANNEL_ID', Integer(), nullable=False, default=0)
    title = Column('TITLE', String(255), nullable=False)
    description = Column('DESCRIPTION', String(255), nullable=True, default=None)
    unread = Column('UNREAD', SmallInteger(), nullable=True, default=None)
    link = Column('LINK', String(255), nullable=True, default=None)
    creator = Column('CREATOR', String(255), nullable=True, default=None)
    subject = Column('SUBJECT', String(255), nullable=True, default=None)
    date = Column('DATE', DateTime(), nullable=True, default=None)
    found = Column('FOUND', DateTime(), nullable=True, default=None)
    guid = Column('GUID', Integer(), nullable=True, default=None)
    comments = Column('COMMENTS', String(255), nullable=True, default=None)
    source = Column('SOURCE', Integer(), nullable=True, default=None)
    enclosure = Column('ENCLOSURE', Integer(), nullable=True, default=None)

    @property
    def parsed_link(self):
        return NewsItem.parse_link(self.link)

    @staticmethod
    def parse_link(link):
        _link_matches = re.findall(r'/iCMS.*_\d{8}_\d{6}', link)
        return _link_matches[0] if _link_matches else None
