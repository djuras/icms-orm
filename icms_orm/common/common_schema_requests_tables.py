from sqlalchemy import Enum
from icms_orm import DeclarativeBase, PseudoEnum
from sqlalchemy.types import Integer, String, Text, DateTime
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB
from datetime import datetime
from icms_orm.common.common_schema_base import CommonBaseMixin
from icms_orm.common.common_schema_people_tables import Person


class RequestStatusValues(PseudoEnum):
    PENDING_APPROVAL = 'PENDING_APPROVAL'
    READY_FOR_EXECUTION = 'READY_FOR_EXECUTION'
    REJECTED = 'REJECTED'
    EXECUTED = 'EXECUTED'
    SCHEDULED_FOR_EXECUTION = 'APPROVED_SCHEDULED'
    FAILED_TO_EXECUTE = 'FAILED_TO_EXECUTE'


request_status_enum = Enum(*RequestStatusValues.values(), name='request_status_enum', inherit_schema=True, metadata=DeclarativeBase.metadata)


class RequestStepStatusValues(PseudoEnum):
    PENDING = 'PENDING'
    APPROVED = 'APPROVED'
    REJECTED = 'REJECTED'


request_step_status_enum = Enum(*RequestStepStatusValues.values(), name='request_step_status_enum', inherit_schema=True, metadata=DeclarativeBase.metadata)


class Request(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    creator_cms_id = Column(Integer, ForeignKey(Person.cms_id, onupdate='CASCADE', ondelete='SET NULL'), nullable=False)
    processing_data = Column(JSONB)
    type = Column(String(64), nullable=False)
    status = Column(request_status_enum, nullable=False)
    scheduled_execution_date = Column(DateTime, nullable=True)
    remarks = Column(Text, nullable=True)
    steps = relationship(lambda: RequestStep, order_by=lambda: RequestStep.id, back_populates='request', primaryjoin=lambda: Request.id == RequestStep.request_id, lazy='joined')


class RequestStep(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    request_id = Column(Integer, ForeignKey(Request.id, onupdate='CASCADE', ondelete='CASCADE'))
    request = relationship(Request, back_populates=Request.steps.key, primaryjoin=lambda: Request.id == RequestStep.request_id, lazy='joined')
    creation_date = Column(DateTime, nullable=False, default=datetime.utcnow)
    creator_cms_id = Column(Integer, ForeignKey(Person.cms_id, onupdate='CASCADE', ondelete='SET NULL'), nullable=True) #, comment='Nullable as some request might be machine-made.')
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    updater_cms_id = Column(Integer, ForeignKey(Person.cms_id, onupdate='CASCADE', ondelete='SET NULL'), nullable=True)
    status = Column(request_step_status_enum, nullable=False)
    deadline = Column(DateTime, nullable=True)
    title = Column(Text, nullable=True)
    remarks = Column(Text, nullable=True)