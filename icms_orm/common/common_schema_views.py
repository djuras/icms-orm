import logging
from typing import Type

from icms_orm import DeclarativeBase, DatabaseViewMixin, cms_common_bind_key
from icms_orm.common.common_schema_base import CommonBaseMixin
from icms_orm import DeclarativeBase
from sqlalchemy.types import Integer, String, Text, DateTime, Boolean, Date, SmallInteger, LargeBinary
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import DDL, event
from icms_orm.common.common_schema_people_tables import person_status_enum


class CommonSchemaViewBase(CommonBaseMixin, DatabaseViewMixin):
    __bind_key__ = CommonBaseMixin.__bind_key__
    __table_args__ = CommonBaseMixin.__table_args__

    @classmethod
    def get_schema_name(cls) -> str:
        return cls.__table_args__.get('schema')


class PersonFlagsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_flags'

    cms_id = Column(Integer, primary_key=True)
    flags = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return 'select cms_id, json_agg(flag_code) as flags from legacy_flag ' + \
               'where now() between start_date and coalesce(end_date, now()) group by cms_id'


class PersonInstCodeView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_inst_code'
    cms_id = Column(Integer, primary_key=True)
    inst_code = Column(String(32))

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return 'select cms_id, inst_code from affiliation where is_primary=true ' + \
               'and now() between start_date and coalesce(end_date, now())'


class PersonAffiliationsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_affiliations'
    cms_id = Column(Integer, primary_key=True)
    affiliations = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select cms_id, json_object_agg(inst_code, case when is_primary then 'primary' else 'secondary' end) " + \
                'as affiliations from affiliation ' + \
                'where now() between start_date and coalesce(end_date, now()) group by cms_id'


class PersonTeamDutiesView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_team_duties'
    cms_id = Column(Integer, primary_key=True)
    team_duties = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select cms_id, json_object_agg(inst_code, case when is_primary=true then 'leader' else 'deputy' end) as team_duties from institute_leader " + \
                "where now() between start_date and coalesce(end_date, now()) " + \
                "group by cms_id"


class PersonManagedUnitIdsView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_managed_unit_ids'
    cms_id = Column(Integer, primary_key=True)
    managed_unit_ids = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select cms_id, json_agg(ou.id) as managed_unit_ids from tenure t " + \
                "join org_unit ou on (t.unit_id = ou.id or t.unit_id = ou.superior_unit_id) " + \
                "join position p on (p.id = t.position_id and p.level <= 1) " + \
                "where now() between t.start_date and coalesce(t.end_date, now()) " + \
                "group by cms_id"


class PersonStatusView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person_status'
    cms_id = Column(Integer, primary_key=True)
    status = Column(person_status_enum)
    activity = Column(String)
    author_block = Column(Boolean)
    is_author = Column(Boolean)
    epr_suspension = Column(Boolean)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select cms_id, status, activity, author_block, is_author, epr_suspension from person_status ps " + \
                "where now() between ps.start_date and coalesce(ps.end_date, now())"


class PersonView(CommonSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_person'
    cms_id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    hr_id = Column(String)
    login = Column(String)
    activity = Column(String)
    is_author = Column(Boolean)
    author_block = Column(Boolean)
    epr_suspension = Column(Boolean)
    affiliations = Column(JSONB)
    team_duties = Column(JSONB)
    flags = Column(JSONB)
    managed_unit_ids = Column(JSONB)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select cms_id, first_name, last_name, hr_id, login, activity, is_author, author_block, epr_suspension, affiliations, team_duties, flags, managed_unit_ids from person " + \
                "left outer join view_person_status using (cms_id) " + \
                "left outer join view_person_affiliations using (cms_id) " + \
                "left outer join view_person_team_duties using (cms_id) " + \
                "left outer join view_person_flags using (cms_id) " + \
                "left outer join view_person_managed_unit_ids using (cms_id)"


for _cls in (DatabaseViewMixin.get_implementing_classes()):
    logging.debug("REGISTERING DDL EVENT FOR CLASS {0}".format(_cls.__name__))
    event.listen(_cls.__table__, 'after_create', DDL(_cls.get_drop_sql().replace(' view ', ' table ')))

