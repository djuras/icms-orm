from icms_orm import cms_common_bind_key, cms_common_schema_name


class CommonBaseMixin(object):
    __bind_key__ = cms_common_bind_key()
    __table_args__ = {'schema': cms_common_schema_name()}