#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0
import os
import re
import logging
from collections import OrderedDict

from alchy import ModelBase, make_declarative_base
from sqlalchemy.engine import Connection
from sqlalchemy.ext.declarative import declared_attr
import stringcase
import sqlalchemy as sa
from sqlalchemy import MetaData, inspect
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy import types
from sqlalchemy.inspection import inspect


def implicit_return_val():
    return os.environ.has_key('TEST_DATABASE_URL')

# Methods below can be overridden in client apps to customise what is necessary, e.g.:
# import icms_orm
# icms_orm.toolkit_schema_name = lambda: 'ICMS_TOOLKIT'

"""
Not to mess the stuff up:
 - CMSPEOPLE and CMSAnalysis "live" on the same bind so the mappers there have to contain some info on the schema
    (and this will probably forever be CMSPEOPLE and CMSAnalysis, the bind's dbstring ends without the schema)
 - toolkit and epr dbs live on their own. no need to specify the schema then
    (but might be necessary to put the schema name at the end of the bind's dbstring)
"""


def toolkit_bind_key():
    return 'icms_toolkit_db'


def toolkit_schema_name():
    return 'toolkit'


def epr_bind_key():
    return 'icms_epr_db'


def epr_schema_name():
    return 'epr'


def cadi_bind_key():
    return cms_people_bind_key()


def cadi_schema_name():
    return 'CMSAnalysis'


def newcadi_bind_key():
    return 'icms_cadi_db'


def newcadi_schema_name():
    return 'cadi'


def cms_people_bind_key():
    return 'icms_legacy_db'


def cms_people_schema_name():
    return 'CMSPEOPLE'


def cms_common_bind_key():
    return 'icms_common_db'


def cms_common_schema_name():
    return 'public'


def old_notes_schema_name():
    return 'Portal_notes'


def old_notes_bind_key():
    return cms_people_bind_key()


def old_notes_wf_schema_name():
    return 'Portal_wf_notes'


def old_wf_notes_bind_key():
    return old_notes_bind_key()


def notes_bind_key():
    return 'notes'


def notes_schema_name():
    return 'notes'


def metadata_schema_name():
    return 'metadata'


def metadata_bind_key():
    return cms_people_bind_key()


def news_bind_key():
    return cms_people_bind_key()


def news_schema_name():
    return 'Portal_news'


__naming_convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

__metadata = MetaData(naming_convention=__naming_convention)


class IcmsModelBase(ModelBase):

    @declared_attr
    def __tablename__(cls):
        return stringcase.snakecase(cls.__name__)

    def __str__(self):
        details = []
        for attr_name, attr_value in self.to_dict().items():
            details.append('%s=%s' % (attr_name, attr_value))
        result = '<%s:%s>' % (self.__class__.__name__, ';'.join(details))
        return result

    def to_dict(self):
        result = OrderedDict()
        for ia in self.ia_list():
            result[ia.key] = self.get(ia)
        return result

    def to_ia_dict(self):
        result = dict()
        for ia in self.ia_list():
            result[ia] = self.get(ia)
        return result

    @classmethod
    def from_ia_dict(cls, ia_dict={}):
        """
        Constructs a mapper object from a dictionary where keys are SQLAlchemy's instrumented attributes
        :param ia_dict: a dictionary with SA instrumented attributes as keys
        :return:
        """
        instance = cls()
        for key, value in ia_dict.items():
            if hasattr(cls, key.key):
                setattr(instance, key.key, value)
        return instance

    @classmethod
    def ia_list(cls):
        """
        :return: A list of instrumented attributes representing the columns of the mapped table
        """
        inspection = inspect(cls)
        ia_list = [c for c in cls.__dict__.values() if isinstance(c, InstrumentedAttribute) and c.key in inspection.c]
        return ia_list

    def get(self, ia):
        return getattr(self, ia.key)

    def set(self, ia, value):
        setattr(self, ia.key, value)

    @classmethod
    def add_unique_key(cls, key_name, *cols_list):
        # starting out with some default that is expected to raise exceptions if we end up with sth else than handled...
        table_args = cls.__table_args__
        if isinstance(table_args, dict):
            # __table_args__ is a dict unless it's not - then it's an elegant list of small dicts and other junk
            table_args = [{_k: _v} for _k, _v in table_args.items()]
        assert isinstance(table_args, list)
        table_args.append(sa.UniqueConstraint(*cols_list, name=key_name))
        cls.__table_args__ = table_args


DeclarativeBase = make_declarative_base(Base=IcmsModelBase, metadata=__metadata)


def c2k(ia):
    """
    Returns a reasonable text representation of provided InstrumentedAttribute instance.
    This can be used in scenarios where a group of columns is queried with intention to subsequently render them on
    the page with some standard, pre-defined description that can be associated with the return value of this function.
    :param ia: instance of InstrumentedAttribute, like Person.cmsId
    :return: a string like 'PeopleData.cmsId'
    """
    return '%s.%s' % (ia.parent.tables[0].key, ia.key)


class PseudoEnum(object):
    @classmethod
    def values(cls):
        return [value for key, value in vars(cls).items() if not (key.startswith('__') or callable(value) or type(value) in (classmethod, staticmethod))]


def translate_bool(something):
    if isinstance(something, str):
        return something.lower() in ('yes', 'true', 'y', 't', '1')
    return bool(something)


def translate_type(something):
    something = something.lower()
    complex_match = re.match('(\w+)(?:\((\d+)\))', something)
    if complex_match:
        _type_name = complex_match.group(1)
        _type_length = int(complex_match.group(2))

        return {
            'varchar': types.String,
            'int': types.Integer,
            'smallint': types.Integer
        }.get(_type_name, None)(*([] if 'int' in _type_name else [_type_length]))
    else:
        return {
            'text': types.Text
        }.get(something)()


class DatabaseViewMixin(object):
    @classmethod
    def get_schema_name(cls) -> str:
        return None

    @classmethod
    def get_view_name(cls) -> str:
        return None

    @classmethod
    def get_sql(cls) -> str:
        return None

    @classmethod
    def get_create_sql(cls):
        return 'create view {schema}.{view_name} as {sql}'.format(schema=cls.get_schema_name(), view_name=cls.get_view_name(), sql=cls.get_sql())

    @classmethod
    def get_drop_sql(cls):
        return 'drop view if exists {schema}.{view_name} cascade'.format(schema=cls.get_schema_name(), view_name=cls.get_view_name())

    @staticmethod
    def get_implementing_classes():
        _list = [DatabaseViewMixin]
        for _cls in _list:
            for _sub_cls in _cls.__subclasses__():
                _list.append(_sub_cls)
        return [_cls for _cls in _list if issubclass(_cls, DeclarativeBase)]

    @staticmethod
    def create_views(connection: Connection, bind=None):
        statements = [_cls.get_create_sql() for _cls in DatabaseViewMixin.get_implementing_classes() if
                     issubclass(_cls, DatabaseViewMixin) and (bind is None or _cls.__table__.info.get('bind_key') == bind)
                    ]
        DatabaseViewMixin.__loop_until_all_execute_or_throw(connection, statements)

    @staticmethod
    def drop_views(connection: Connection, bind=None):
        statements = [_cls.get_drop_sql() for _cls in DatabaseViewMixin.get_implementing_classes() if
                      issubclass(_cls, DatabaseViewMixin) and (bind is None or _cls.__table__.info.get('bind_key') == bind)
                    ]
        DatabaseViewMixin.__loop_until_all_execute_or_throw(connection, statements)

    @staticmethod
    def __loop_until_all_execute_or_throw(connection: Connection, statements: [str]):
        remaining_statements = []
        while len(statements) > 0:
            logging.info('There are {0} statements to execute'.format(len(statements)))
            last_exception = None
            for stmt in statements:
                try:
                    connection.execute(stmt)
                except Exception as e:
                    last_exception = e
                    remaining_statements.append(stmt)
            if len(statements) > len(remaining_statements):
                statements = remaining_statements
            else:
                if last_exception is not None:
                    raise last_exception
                else:
                    raise Exception('Looping with {0} statements left to execute'.format(len(statements)))