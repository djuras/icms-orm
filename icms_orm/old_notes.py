#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import String, Integer, Date, Enum, Float, Text, SmallInteger, DateTime
from icms_orm import old_notes_bind_key, old_notes_schema_name, DeclarativeBase, old_notes_wf_schema_name
from type_decorators import StringDatetime
from .cmspeople import Person
import logging, traceback, sqlalchemy as sa
import datetime


class OldNotesBaseMixin(object):
    __bind_key__ = old_notes_bind_key()
    __table_args__ = {'schema': old_notes_schema_name()}


class OldPortalWfNotesBaseMixin(object):
    __bind_key__ = old_notes_bind_key()
    __table_args__ = {'schema': old_notes_wf_schema_name()}


class Note(OldNotesBaseMixin, DeclarativeBase):
    __tablename__ = 'Note'

    id = Column(Integer, primary_key=True, autoincrement=True)
    cmsNoteId = Column('CMSNoteID', String(17), nullable=False, unique=True)
    title = Column('title', Text, nullable=False)
    subdetector = Column('subdetector', String(30), nullable=True)
    submitter = Column('submitter', Integer, nullable=False)
    submitterName = Column('submitterName', String(255), nullable=True)
    submitDate = Column('submitDate', DateTime, nullable=False)
    type = Column('type', String(4), nullable=False)
    status = Column('status', String(10), nullable=True)
    URL = Column('URL', String(255), nullable=False)
    abstr = Column('abstr', Text, nullable=False)
    authors = Column('authors', Text, nullable=False)
    additionalContact = Column('additionalContact', String(255), nullable=True)
    modificationDate = Column('modifDate', DateTime, nullable=True)
    relatedTo = Column('relatedTo', String(255), nullable=True)
    publishedIn = Column('publishedIn', String(255), nullable=True)
    remarks = Column('remarks', Text)

    @classmethod
    def get_subdetectors(cls):
        _res = []
        try:
            _res = sorted([v[0] for v in Note.session().query(sa.distinct(Note.subdetector)).all()])
        except Exception as e:
            logging.error(e)
        return _res

class NoteFile(OldNotesBaseMixin, DeclarativeBase):
    __tablename__ = 'NoteFiles'

    id = Column(Integer, primary_key=True, autoincrement=True)
    noteId = Column('article', Integer, ForeignKey(Note.id), nullable=False)
    modifDate = Column('modifDate', DateTime, nullable=False)
    fileName = Column('fileName', String(100), nullable=False)


class WorkflowProcess(OldPortalWfNotesBaseMixin, DeclarativeBase):
    __tablename__ = 'Process'

    id = Column('id', Integer(), primary_key=True, autoincrement=True)
    startDatetime = Column('startDateStr', StringDatetime(length=255, format='%Y/%m/%d %H:%M:%S'), nullable=False)
    endDatetime = Column('endDateStr', StringDatetime(length=255, format='%Y/%m/%d %H:%M:%S'), nullable=False)
    type = Column('type', String(255), nullable=False)
    status = Column('status', String(255), nullable=False)
    submitter = Column('submitter', Integer(), nullable=False)
    submitterName = Column('submitterName', String(255), nullable=False)


class WorkflowData(OldPortalWfNotesBaseMixin, DeclarativeBase):
    __tablename__ = 'Data'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    cmsNoteId = Column('CMSNoteID', String(17))
    subeditor = Column('subeditor', Integer(), nullable=True)
    subeditorName = Column('subeditorName', String(255), nullable=True)
    editor = Column('editor', Integer(), nullable=True)
    editorName = Column('editorName', String(255), nullable=True)
    refCMSid = Column('refCMSid', String(17), nullable=False)
    referee = Column('referee', String(255), nullable=False)
    title = Column('title', Text, nullable=False)
    abstr = Column('abstr', Text, nullable=False)
    authors = Column('authors', Text, nullable=False)
    modificationDatetime = Column('modifDate', StringDatetime(length=255, format='%Y/%m/%d %H:%M:%S'), nullable=True)
    isRedirectable = Column('isRedirectable', String(255), nullable=True)
    relatedTo = Column('relatedTo', String(255), nullable=True)
    publishedIn = Column('publishedIn', String(255), nullable=True)
    subproject = Column('subproject', String(255), nullable=True)
    remarks = Column('remarks', Text, nullable=True)
    status = Column('status', String(10), nullable=True)
    categories = Column('categories', String(255), nullable=False)
    fileName = Column('fileName', String(255), nullable=False)
    accepted = Column('accepted', String(255), nullable=True)
    acceptedByEditor = Column('acceptedByEditor', String(255), nullable=True)
    acceptedBySubeditor = Column('acceptedBySubeditor', String(255), nullable=True)
    acceptedBySecr = Column('acceptedBySecr', String(255), nullable=True)


class WorkflowRecord(OldPortalWfNotesBaseMixin, DeclarativeBase):
    __tablename__ = 'WorkflowRecord'

    id = Column(Integer, primary_key=True, autoincrement=True)
    dateString = Column('dateStr', String(100))
    status = Column('status', String(100))
    acquiredBy = Column('acquiredBy', String(100))
    operationReceived = Column('opReceived', String(100))
    operationAwaited = Column('opWaitFor', String(100))
    state = Column('state', String(100))
    process = Column(Integer)
