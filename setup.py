#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from setuptools import setup, find_packages

from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='icms_orm',
    version='0.4',
    description='SQL Alchemy mappings for iCMS webapps',
    long_description='This project serves as a library of DB model objects to be used across other applications forming the iCMS suite.',
    url='https://gitlab.cern.ch/cms-icmsweb/icms-orm',
    author='iCMS-support',
    author_email='icms-support@cern.ch',
    license='Apache-2.0',
    classifiers=[
        'Development Status :: 5 - Production',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: ORM definitions',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='CERN iCMS orm sqlalchemy',
    packages=find_packages(exclude=['docs', 'tests']),
    install_requires=['sqlalchemy>=1.3.1',
                      'alchy>=2.2.2',
                      'PyMySQL>=0.9.3',
                      'python-dateutil>=2.8.0',
                      'psycopg2>=2.7.7',
                      'stringcase>=1.2.0',
                      ],
    entry_points={}
)
