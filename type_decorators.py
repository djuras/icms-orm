#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy.types import TypeDecorator, String
from dateutil import parser as dateparser
from datetime import datetime

class StringInt(TypeDecorator):
    impl = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw['length'])

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(value)

    def process_result_value(self, value, dialect):
        if value is not None and len(value.strip()) > 0:
            return int(value)
        return 0


class StringFloat(TypeDecorator):
    impl = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw['length'])

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(value)

    def process_result_value(self, value, dialect):
        if isinstance(value, float):
            return value
        if value is not None and len(value.strip()) > 0:
            return float(value)
        return 0.0


class StringBool(TypeDecorator):
    impl = String(1)

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(int(value))

    def process_result_value(self, value, dialect):
        if value in ['0', '1']:
            return bool(int(value))
        return False


class StringDatetime(TypeDecorator):
    impl = None
    format = '%Y/%m/%d %H:%M'
    custom_format = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw.get('length', arg and arg[0] or String))
        if 'format' in kw:
            self.custom_format = kw['format']

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return value.strftime(format=self.format)

    def process_result_value(self, value, dialect):
        try:
            if value is None or str(value) == '':
                return None
            if self.custom_format:
                return datetime.strptime(value, self.custom_format)
            else:
                return dateparser.parse(value)
        except ValueError:
            return None


class StringDate(TypeDecorator):
    impl = None
    format = '%Y/%m/%d'
    custom_format = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw.get('length', arg and arg[0] or String))
        if 'format' in kw:
            self.custom_format = kw['format']

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return value.strftime(format=self.format)

    def process_result_value(self, value, dialect):
        try:
            if value is None or str(value) == '':
                return None
            if self.custom_format:
                return datetime.strptime(value, self.custom_format).date()
            else:
                return dateparser.parse(value)
        except ValueError:
            return None
