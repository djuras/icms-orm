#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import icms_orm
from alchy import Manager
icms_orm.toolkit_schema_name = lambda: None

from icms_orm import toolkit, DeclarativeBase

test_config = {
        'SQLALCHEMY_BINDS': {
            icms_orm.epr_bind_key(): 'sqlite:////tmp/epr.sqlite',
            icms_orm.toolkit_bind_key(): 'sqlite:////tmp/toolkit.sqlite'
        }
}

db = Manager(config=test_config, Model=DeclarativeBase)

def fill_db():
    db.drop_all()
    db.create_all()

    for id, status_name in enumerate(['Member', 'Associate', 'Observer']):
        status = toolkit.CernCountryStatus(name=status_name)
        status.id = id
        db.session.add(status)
    db.session.commit()


def test_create_databases():
    fill_db()
    assert db.session.query(toolkit.CernCountryStatus).count() > 0
    assert db.session.query(toolkit.CernCountry).count() == 0


