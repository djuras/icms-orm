"""
Purpose: test if accessing databases on different binds can work with a single manager object
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import icms_orm
from test_config import test_config
from alchy import Manager
from icms_orm.epr import TimeLineUser
from icms_orm.cmspeople.people import Person
from icms_orm.toolkit import AuthorshipApplicationCheck

db = Manager(config=test_config)

def _do_not_test_join_epr_against_people():
    """
    This is unlikely to work as the schemas reside in separate databases
    :return:
    """
    results = db.session.query(TimeLineUser, Person).join(Person, TimeLineUser.cmsId == Person.cmsId).\
        filter(TimeLineUser.year == 2017).filter(Person.cmsId.in_([9981, 10201, 123])).all()
    assert len(results)>0

def test_join_epr_against_toolkit():
    """
    This should work if the same database server is used for epr and toolkit schemas and if the correct rights are assigned
    :return:
    """

    results = db.session.query(AuthorshipApplicationCheck, TimeLineUser).\
        join(TimeLineUser, AuthorshipApplicationCheck.cmsId == TimeLineUser.cmsId).filter(TimeLineUser.year == 2017).\
        all()

    assert len(results)>0


def test_epr_classes_have_correct_metadata():
    assert hasattr(TimeLineUser, '__bind_key__')
    assert TimeLineUser.__bind_key__ == icms_orm.epr_bind_key()

    assert hasattr(TimeLineUser, '__table_args__')
    table_args = TimeLineUser.__table_args__
    assert 'schema' in table_args
    assert table_args['schema'] == icms_orm.epr_schema_name()


def test_toolkit_classes_have_correct_metadata():
    assert hasattr(AuthorshipApplicationCheck, '__bind_key__')
    assert AuthorshipApplicationCheck.__bind_key__ == icms_orm.toolkit_bind_key()

    assert hasattr(AuthorshipApplicationCheck, '__table_args__')
    table_args = AuthorshipApplicationCheck.__table_args__
    assert 'schema' in table_args
    assert table_args['schema'] == icms_orm.toolkit_schema_name()


def test_if_toolkit_manager_can_join_against_epr():
    results = db.session.query(AuthorshipApplicationCheck, TimeLineUser).\
        join(TimeLineUser, AuthorshipApplicationCheck.cmsId == TimeLineUser.cmsId).filter(TimeLineUser.year == 2017).\
        all()
    assert len(results) > 0
