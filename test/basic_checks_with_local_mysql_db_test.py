#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import icms_orm
from test_config import test_config

from icms_orm import epr, toolkit, cmspeople, cmsanalysis
from alchy import Manager
from pytest import raises
from icms_orm import DeclarativeBase

db = Manager(config=test_config, Model=DeclarativeBase)


def test_should_be_able_to_retrieve_something_from_local_epr_database():
    assert db.session.query(epr.EprInstitute).count() > 0


def test_should_be_able_to_retrieve_something_from_local_toolkit_database():
    assert db.session.query(toolkit.CernCountry).count() > 0


def test_should_fail_due_to_overriding_bind_key():
    WrongInstitute = type('Institute', (epr.EprInstitute,), {'__bind_key__': 'rubbish'})
    with raises(AssertionError):
        db.session.query(WrongInstitute).count()

    BetterInstitute = type('Institute', (WrongInstitute, ), {'__bind_key__': icms_orm.epr_bind_key()})
    assert db.session.query(BetterInstitute).count() > 0


def test_should_be_able_to_fetch_some_people_from_icms_database():
    assert db.session.query(cmspeople.Person).count() > 0


def test_should_be_able_to_get_some_flags_for_any_aperson():
    person = db.session.query(cmspeople.Person).filter(cmspeople.Person.cmsId == 9981).one()
    assert len(person.flags) > 0
    for flag in person.flags:
        assert len(flag.people) > 0
        print('%s flag is set for %d peple' % (flag, len(flag.people)))


def test_should_be_able_to_find_some_analyses():
    assert db.session.query(cmsanalysis.CadiAnalysis).count() > 0

