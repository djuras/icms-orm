#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import icms_orm

icms_orm.cms_people_schema_name = lambda: 'TEST_CMSPEOPLE'
icms_orm.cadi_schema_name = lambda: 'TEST_CMSAnalysis'

from test_config import test_config

for key in [icms_orm.cms_people_bind_key(), icms_orm.cadi_bind_key()]:
    test_config['SQLALCHEMY_BINDS'][key] = test_config['SQLALCHEMY_BINDS'][key].\
        replace('//icms_legacy@', '//icms_legacy_test@')

from icms_orm import epr, toolkit, cmspeople, cmsanalysis
from alchy import Manager
from pytest import raises


db = Manager(config=test_config, Model=icms_orm.DeclarativeBase)


def test_should_fail_due_to_overriding_bind_key():
    WrongInstitute = type('Institute', (epr.Institute,), {'__bind_key__': 'rubbish'})
    with raises(AssertionError):
        db.session.query(WrongInstitute).count()

    BetterInstitute = type('Institute', (WrongInstitute, ), {'__bind_key__': icms_orm.epr_bind_key()})
    assert db.session.query(BetterInstitute).count() > 0


def test_should_be_able_to_fetch_some_people_from_icms_database():
    assert db.session.query(cmspeople.Person).count() > 0


def test_schema_names_have_been_altered():
    assert icms_orm.cms_people_schema_name() == 'TEST_CMSPEOPLE'
    assert cmspeople.Person.__table_args__['schema'] == 'TEST_CMSPEOPLE'
    assert cmspeople.Institute.__table_args__['schema'] == 'TEST_CMSPEOPLE'
    assert icms_orm.cadi_schema_name() == 'TEST_CMSAnalysis'
    assert cmsanalysis.Analysis.__table_args__['schema'] == 'TEST_CMSAnalysis'


def test_should_be_able_to_get_some_flags_for_any_aperson():
    person = db.session.query(cmspeople.Person).filter(cmspeople.Person.cmsId==9981).one()
    assert len(person.flags) > 0
    for flag in person.flags:
        assert len(flag.people) > 0
        print('%s flag is set for %d peple' % (flag, len(flag.people)))

