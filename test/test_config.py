#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import icms_orm

test_config = {
        'SQLALCHEMY_BINDS': {
            icms_orm.toolkit_bind_key(): 'postgresql+psycopg2://toolkit:icms@localhost/icms',
            icms_orm.epr_bind_key(): 'postgresql+psycopg2://epr:icms@localhost/icms',
            icms_orm.cms_people_bind_key(): 'mysql+pymysql://icms_legacy@localhost/?charset=utf8',
            icms_orm.cadi_bind_key(): 'mysql+pymysql://icms_legacy@localhost/?charset=utf8',
            icms_orm.cms_common_bind_key(): 'postgresql+psycopg2://icms:icms@localhost/icms'
        }
}
