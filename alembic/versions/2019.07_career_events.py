"""career events

Revision ID: 2019.07
Revises: 2019.06
Create Date: 2019-07-15 12:09:52.788043

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from icms_orm import DeclarativeBase
from icms_orm.common import career_event_type_enum


# revision identifiers, used by Alembic.
revision = '2019.07'
down_revision = '2019.06'
branch_labels = None
depends_on = None


def upgrade():
    career_event_type_enum.create(op.get_bind())

    op.create_table('career_event',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('cms_id', sa.Integer(), nullable=False),
        sa.Column('date', sa.Date(), nullable=False),
        sa.Column('event_type', career_event_type_enum, nullable=False),
        sa.ForeignKeyConstraint(['cms_id'], [u'public.person.cms_id'], name=op.f('fk_career_event_cms_id_person')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_career_event')),
        schema='public'
    )


def downgrade():
    op.drop_table('career_event', schema='public')
    career_event_type_enum.drop(op.get_bind())
