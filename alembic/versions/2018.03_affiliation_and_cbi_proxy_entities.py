"""Affiliation and CBI proxy entities

Revision ID: 2018.03
Revises: 2018.02
Create Date: 2018-02-13 15:00:05.147299

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2018.03'
down_revision = '2018.02'
branch_labels = None
depends_on = None


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('affiliation',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('cms_id', sa.Integer(), nullable=False),
    sa.Column('inst_code', sa.String(length=32), nullable=False),
    sa.Column('is_primary', sa.Boolean(), nullable=False),
    sa.Column('start_date', sa.Date(), nullable=False),
    sa.Column('end_date', sa.Date(), nullable=True),
    sa.ForeignKeyConstraint(['cms_id'], [u'public.person.cms_id'], name=op.f('fk_affiliation_cms_id_person'), onupdate='cascade', ondelete='cascade'),
    sa.ForeignKeyConstraint(['inst_code'], [u'public.institute.code'], name=op.f('fk_affiliation_inst_code_institute'), onupdate='cascade', ondelete='cascade'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_affiliation')),
    schema='public'
    )
    op.create_table('institute_leader',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('cms_id', sa.Integer(), nullable=False),
    sa.Column('inst_code', sa.String(length=32), nullable=False),
    sa.Column('is_primary', sa.Boolean(), nullable=False),
    sa.Column('start_date', sa.Date(), nullable=False),
    sa.Column('end_date', sa.Date(), nullable=True),
    sa.ForeignKeyConstraint(['cms_id'], [u'public.person.cms_id'], name=op.f('fk_institute_leader_cms_id_person'), onupdate='cascade', ondelete='cascade'),
    sa.ForeignKeyConstraint(['inst_code'], [u'public.institute.code'], name=op.f('fk_institute_leader_inst_code_institute'), onupdate='cascade', ondelete='cascade'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_institute_leader')),
    schema='public'
    )

    op.add_column('mo', sa.Column('listed', sa.Boolean(), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###

    op.drop_column('mo', 'listed')
    op.drop_table('institute_leader', schema='public')
    op.drop_table('affiliation', schema='public')
    ### end Alembic commands ###
